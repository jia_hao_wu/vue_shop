import Vue from 'vue'
import VueRouter from 'vue-router'

const Login = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Login.vue')
const Home = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Home.vue')
const Welcome = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Welcome.vue')

const Users = () => import(/* webpackChunkName: "users_rights_roles" */ '../components/user/Users.vue')
const Rights = () => import(/* webpackChunkName: "users_rights_roles" */ '../components/power/Rights.vue')
const Roles = () => import(/* webpackChunkName: "users_rights_roles" */ '../components/power/Roles.vue')

const Cate = () => import(/* webpackChunkName: "cate_params" */ '../components/goods/Cate.vue')
const Params = () => import(/* webpackChunkName: "cate_params" */ '../components/goods/Params.vue')

const GoodsList = () => import(/* webpackChunkName: "goodslist_addgoods" */ '../components/goods/List.vue')
const AddGoods = () => import(/* webpackChunkName: "goodslist_addgoods" */ '../components/goods/addGoods.vue')

const Order = () => import(/* webpackChunkName: "order_report" */ '../components/order/Order.vue')
const Report = () => import(/* webpackChunkName: "order_report" */ '../components/report/Report.vue')

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/home' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users, name: '用户列表' },
      { path: '/rights', component: Rights, name: '权限列表' },
      { path: '/roles', component: Roles, name: '角色列表' },
      { path: '/categories', component: Cate, name: '商品分类' },
      { path: '/params', component: Params, name: '分类参数' },
      { path: '/goods', component: GoodsList, name: '商品列表' },
      { path: '/goods/add', component: AddGoods, name: '添加商品' },
      { path: '/orders', component: Order, name: '订单列表' },
      { path: '/reports', component: Report, name: '数据报表' }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 代表 将要访问的路径;
  // from 代表 从哪个路径跳转而来;
  // next 是一个函数，表示放行;
  //      next() 放行   next('/login')  强制跳转
  if (to.path === '/login') return next()
  // 获取 token ;
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
